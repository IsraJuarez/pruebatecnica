package com.freektzon.pruebatecnica.objects

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.freektzon.pruebatecnica.utils.Converters

@Entity(tableName = "pokemon")
data class Poke(@PrimaryKey(autoGenerate = false)val name:String, var url:String, val id: Int=0, var weight:String?, var height:String?, var types:ArrayList<Types>?, var isFavorite:Boolean, var sprites: Sprites?) {
}