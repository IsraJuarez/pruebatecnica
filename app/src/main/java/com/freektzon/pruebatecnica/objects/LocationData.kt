package com.freektzon.pruebatecnica.objects

data class LocationData(val latitude:Double,val longitude:Double, val timestamp: Any)
