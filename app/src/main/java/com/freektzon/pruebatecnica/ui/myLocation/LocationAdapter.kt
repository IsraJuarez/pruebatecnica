package com.freektzon.pruebatecnica.ui.myLocation

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.freektzon.pruebatecnica.R
import com.freektzon.pruebatecnica.databinding.ItemLocationsListBinding
import com.freektzon.pruebatecnica.databinding.ItemPokeBinding
import com.freektzon.pruebatecnica.objects.LocationData
import com.freektzon.pruebatecnica.ui.listPokes.ListPokesAdapter

class LocationAdapter(private val list:List<LocationData>) : RecyclerView.Adapter<LocationAdapter.ViewHolder>() {

    class ViewHolder(val binding: ItemLocationsListBinding) : RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemLocationsListBinding = DataBindingUtil.inflate(layoutInflater, R.layout.item_locations_list, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.binding.txtFecha.text = item.timestamp.toString()
        holder.binding.txtLatitude.text = "Latitude: ${item.latitude}"
        holder.binding.txtLongitude.text = "Longitude: ${item.longitude}"
    }
}