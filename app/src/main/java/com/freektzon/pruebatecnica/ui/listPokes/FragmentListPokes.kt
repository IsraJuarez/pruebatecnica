package com.freektzon.pruebatecnica.ui.listPokes

import android.annotation.SuppressLint
import android.content.Intent
import android.nfc.tech.MifareUltralight
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.freektzon.pruebatecnica.databinding.FragmentListPokesBinding
import com.freektzon.pruebatecnica.repository.Repository
import com.freektzon.pruebatecnica.repository.RetrofitClient
import com.freektzon.pruebatecnica.services.PokeApiServices
import com.freektzon.pruebatecnica.ui.myLocation.MyLocation
import com.freektzon.pruebatecnica.utils.MyLoader
import com.freektzon.pruebatecnica.viewModel.mainViewModel.MainViewModel
import com.freektzon.pruebatecnica.viewModel.mainViewModel.ViewModelFactory

@Suppress("NAME_SHADOWING")
class FragmentListPokes : Fragment() {

    private lateinit var binding: FragmentListPokesBinding
    private lateinit var viewModel: MainViewModel
    private lateinit var loader: MyLoader

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentListPokesBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val apiService = RetrofitClient.instance.create(PokeApiServices::class.java)
        val repository = Repository(apiService)
        viewModel = ViewModelProvider(
            this,
            ViewModelFactory(requireActivity().application, repository)
        )[MainViewModel::class.java]
        viewModel.fetchData()
    }

    @SuppressLint("ClickableViewAccessibility", "NotifyDataSetChanged")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loader = MyLoader(binding.progresB)
        binding.lifecycleOwner = this
        viewModel.getListPokemon()
        setUpEvents()
    }

    private fun setUpEvents() {
        binding.btnMyLocation.setOnClickListener(){
            startActivity(Intent(requireContext(),MyLocation::class.java))
        }
        viewModel.status.observe(viewLifecycleOwner, Observer{ if (it) loader.show() else loader.hide() })
        viewModel.data.observe(viewLifecycleOwner, Observer {
            var isLoading = false
            val layoutManager = binding.recyclerListPokes.layoutManager as? GridLayoutManager ?: return@Observer
            val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
            val adapter = ListPokesAdapter(it, requireContext())
            adapter.clickListener = { it ->
                findNavController().navigate(
                    FragmentListPokesDirections.actionFragmentListPokesToFragmentDetailsPoke2(url = it.url, name = it.name)
                )
            }
            adapter.clickFavorite = {pokemon ->
                viewModel.updateStatusFavorite(pokemon)
            }
            binding.recyclerListPokes.adapter = adapter
            layoutManager.scrollToPosition(firstVisibleItemPosition)
            binding.recyclerListPokes.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)

                    val visibleItemCount = layoutManager.childCount
                    val totalItemCount = layoutManager.itemCount
                    val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
                    if (!isLoading) {
                        if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= MifareUltralight.PAGE_SIZE
                        ) {
                            viewModel.fetchData()
                            isLoading = true
                        }
                    }
                }
            })
        })
    }
}