package com.freektzon.pruebatecnica.ui.pokeDetails

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.freektzon.pruebatecnica.R
import com.freektzon.pruebatecnica.databinding.FragmentDetailsPokeBinding
import com.freektzon.pruebatecnica.objects.Poke
import com.freektzon.pruebatecnica.repository.Repository
import com.freektzon.pruebatecnica.repository.RetrofitClient
import com.freektzon.pruebatecnica.services.PokeApiServices
import com.freektzon.pruebatecnica.utils.MyLoader
import com.freektzon.pruebatecnica.viewModel.detailViewModel.DetailViewModel
import com.freektzon.pruebatecnica.viewModel.detailViewModel.DetailViewModelFactory

class FragmentDetailsPoke:Fragment() {
    private val args:FragmentDetailsPokeArgs by navArgs()
    val handler = Handler(Looper.getMainLooper())
    private lateinit var binding: FragmentDetailsPokeBinding
    private lateinit var viewModel: DetailViewModel
    private lateinit var loader: MyLoader
    private lateinit var pokeMon :Poke
    private var url = ""
    private var name = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailsPokeBinding.inflate(layoutInflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        url = args.url
        name = args.name
        loader = MyLoader(binding.progresB)
        val apiService = RetrofitClient.instance.create(PokeApiServices::class.java)
        val repository = Repository(apiService)
        viewModel = ViewModelProvider(this, DetailViewModelFactory(requireActivity().application,repository,url, name))[DetailViewModel::class.java]
        binding.lifecycleOwner = this
        setUpEvents()
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    fun setUpEvents(){
        viewModel.status.observe(viewLifecycleOwner, Observer{ if (it){
            loader.show()
            binding.cardV.visibility = View.GONE
            binding.buttonFavorite.visibility = View.GONE
        } else {
            loader.hide()
            binding.cardV.visibility = View.VISIBLE
            binding.buttonFavorite.visibility = View.VISIBLE
        }
        })
        viewModel.data.observe(viewLifecycleOwner, Observer {
            pokeMon = it
                var types = ""
                for (poke in pokeMon.types!!){
                    if (poke.type != null){
                        types = if (types.isEmpty()) poke.type.name.toString() else types +" - "+poke.type.name
                    }
                }
                pokeMon.sprites?.frontDefault?.let {
                        it1 -> setImage(it1,name)
                }?: kotlin.run {
                    setImage(pokeMon.url,pokeMon.name)
                }
                binding.poke = pokeMon
                binding.types = types
                binding.buttonFavorite.setImageDrawable(
                    requireContext().getDrawable(if (pokeMon.isFavorite)R.drawable.image_favorite
                    else R.drawable.image_notfavorite)
                )
        })
        viewModel.getDetailsPoke()
        binding.buttonFavorite.setOnClickListener(){
            val scaleAnimation = AnimationUtils.loadAnimation(context, R.anim.scale_up)
            binding.buttonFavorite.startAnimation(scaleAnimation)
            pokeMon.isFavorite = !pokeMon.isFavorite
            binding.buttonFavorite.setImageDrawable(
                requireContext().getDrawable(if (pokeMon.isFavorite)R.drawable.image_favorite
                else R.drawable.image_notfavorite))
            viewModel.updateStatusFavorite(pokeMon)
        }
    }

    private fun setImage(url: String, initialName: String) {
        val backgroundColor = Color.parseColor("#FF000000")
        val colorDrawable = ColorDrawable(backgroundColor)
        if (!url.isNullOrEmpty()) {
            Glide.with(binding.imagePoke.context)
                .load(url)
                .listener(object : RequestListener<Drawable?> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any,
                        target: Target<Drawable?>,
                        isFirstResource: Boolean
                    ): Boolean {
                        handler.post {
                            if (starsWithLetters(initialName)) {
                                binding.imagePoke.alpha = 0f
                                Glide.with(binding.imagePoke.context)
                                    .load(colorDrawable)
                                    .into(binding.imagePoke)
                                binding.imagePoke.animate().setDuration(300).alpha(1f)
                                    .start()
                                binding.txtInitialsName.text = getInitials(initialName)
                            } else {
                                Glide.with(binding.imagePoke.context)
                                    .load(R.drawable.image_placeholder)
                                    .into(binding.imagePoke)
                            }
                        }
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any,
                        target: Target<Drawable?>,
                        dataSource: DataSource,
                        isFirstResource: Boolean
                    ): Boolean {
                        binding.imagePoke.animate().setDuration(300).alpha(1f).start()
                        return false
                    }
                })
                .into(binding.imagePoke)
        } else if (!initialName.isNullOrEmpty() && starsWithLetters(initialName)) {
            binding.imagePoke.alpha = 0f
            Glide.with(binding.imagePoke.context)
                .load(colorDrawable)
                .into(binding.imagePoke)
            binding.imagePoke.animate().setDuration(300).alpha(1f).start()
            binding.txtInitialsName.text = getInitials(initialName)
        } else {
            Glide.with(binding.imagePoke.context)
                .load(R.drawable.image_placeholder)
                .into(binding.imagePoke)
        }
    }

    private fun getInitials(name: String): String {
        var initials = ""
        val palabras = name.split(Regex("\\s+"))
        if (palabras.size > 1) {
            for (palabra in palabras.take(2)) {
                initials += palabra[0]
            }
        } else {
            val palabra = palabras[0]
            initials = palabra[0].toString()
        }
        return initials.uppercase()
    }

    fun starsWithLetters(str: String): Boolean {
        val patron = Regex("^[A-Za-z]+.*$")
        return patron.matches(getInitials(str))
    }
}