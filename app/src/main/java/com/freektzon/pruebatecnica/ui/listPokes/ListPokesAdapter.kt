package com.freektzon.pruebatecnica.ui.listPokes

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.freektzon.pruebatecnica.R
import com.freektzon.pruebatecnica.databinding.ItemPokeBinding
import com.freektzon.pruebatecnica.objects.Poke
import com.freektzon.pruebatecnica.utils.Constants


class ListPokesAdapter(private val listPokes: List<Poke>?, private val context:Context) :
    RecyclerView.Adapter<ListPokesAdapter.ViewHolder>() {

    lateinit var clickListener: (Poke) -> Unit
    lateinit var clickFavorite: (Poke) -> Unit
    val handler = Handler(Looper.getMainLooper())

    class ViewHolder(val binding: ItemPokeBinding) : RecyclerView.ViewHolder(binding.root) {

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemPokeBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.item_poke, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return listPokes?.size ?: 0
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var item = listPokes!![position]
        val parts = item.url.split("/")
        val id = parts[parts.size - 2]
        val url = Constants.BASE_URL_IMAGE +id+ ".png"
        holder.binding.txtName.text = item.name
        val initialName = item.name
        setImage(holder, url, initialName)
        holder.binding.contentItem.setOnClickListener {
                clickListener(item)
        }
        holder.binding.buttonFavorite.setImageDrawable(context.getDrawable(
            if (item.isFavorite) R.drawable.image_favorite else R.drawable.image_notfavorite))
        holder.binding.buttonFavorite.setOnClickListener(){
            val scaleAnimation = AnimationUtils.loadAnimation(context, R.anim.scale_up)
            holder.binding.buttonFavorite.startAnimation(scaleAnimation)
            item.isFavorite = !item.isFavorite
            holder.binding.buttonFavorite.setImageDrawable(context.getDrawable(
                if (item.isFavorite)R.drawable.image_favorite else R.drawable.image_notfavorite))
            clickFavorite(item)
        }
    }

    private fun setImage(holder: ViewHolder, url: String, initialName: String) {
        val backgroundColor = Color.parseColor("#FF000000")
        val colorDrawable = ColorDrawable(backgroundColor)
        if (!url.isNullOrEmpty()) {
            Glide.with(holder.binding.imageView.context)
                .load(url)
                .listener(object : RequestListener<Drawable?> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any,
                        target: Target<Drawable?>,
                        isFirstResource: Boolean
                    ): Boolean {
                        handler.post {
                            if (starsWithLetters(initialName)) {
                                holder.binding.imageView.alpha = 0f
                                Glide.with(holder.binding.imageView.context)
                                    .load(colorDrawable)
                                    .into(holder.binding.imageView)
                                holder.binding.imageView.animate().setDuration(300).alpha(1f)
                                    .start()
                                holder.binding.txtInitialsName.text = getInitials(initialName)
                            } else {
                                Glide.with(holder.binding.imageView.context)
                                    .load(R.drawable.image_placeholder)
                                    .into(holder.binding.imageView)
                            }
                        }
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any,
                        target: Target<Drawable?>,
                        dataSource: DataSource,
                        isFirstResource: Boolean
                    ): Boolean {
                        holder.binding.imageView.animate().setDuration(300).alpha(1f).start()
                        return false
                    }
                })
                .into(holder.binding.imageView)
        } else if (!initialName.isNullOrEmpty() && starsWithLetters(initialName)) {
            holder.binding.imageView.alpha = 0f
            Glide.with(holder.binding.imageView.context)
                .load(colorDrawable)
                .into(holder.binding.imageView)
            holder.binding.imageView.animate().setDuration(300).alpha(1f).start()
            holder.binding.txtInitialsName.text = getInitials(initialName)
        } else {
            Glide.with(holder.binding.imageView.context)
                .load(R.drawable.image_placeholder)
                .into(holder.binding.imageView)
        }
    }

    private fun getInitials(name: String): String {
        var initials = ""
        val words = name.split(Regex("\\s+"))
        if (words.size > 1) {
            for (palabra in words.take(2)) {
                initials += palabra[0]
            }
        } else {
            val word = words[0]
            initials = word[0].toString()
        }
        return initials.uppercase()
    }

    fun starsWithLetters(str: String): Boolean {
        val patron = Regex("^[A-Za-z]+.*$")
        return patron.matches(getInitials(str))
    }
}