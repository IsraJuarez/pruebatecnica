package com.freektzon.pruebatecnica.ui.myLocation

import android.Manifest
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.freektzon.pruebatecnica.R
import com.freektzon.pruebatecnica.databinding.ActivityMyLocationBinding
import com.freektzon.pruebatecnica.objects.LocationData
import com.freektzon.pruebatecnica.viewModel.locationViewModel.LocationViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore

class MyLocation : AppCompatActivity(), OnMapReadyCallback {
    private lateinit var binding: ActivityMyLocationBinding
    private lateinit var fusedLocation:FusedLocationProviderClient
    private val db = FirebaseFirestore.getInstance()
    private lateinit var viewModel: LocationViewModel
    val handler = Handler(Looper.getMainLooper())
    private lateinit var mMap: GoogleMap
    private val delayMillis:Long = 2 * 60 * 1000 // 2 minutos en milisegundos
    private var permissionRequestCount = 0
    private val maxPermissionRequests = 3
    private val requestCode = 123
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMyLocationBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (savedInstanceState != null) {
            permissionRequestCount = savedInstanceState.getInt("permissionRequestCount", 0)
        }
        viewModel = ViewModelProvider(this)[LocationViewModel::class.java]
        binding.lifecycleOwner = this
        fusedLocation = LocationServices.getFusedLocationProviderClient(this)
        val mapFragment = supportFragmentManager.findFragmentById(R.id.mapFragment) as SupportMapFragment
        mapFragment.getMapAsync(this)

        requestLocationAndPermission()

    }


    private fun requestLocationAndPermission() {
        if (hasLocationPermission()) {
            startRepeatingTask()
        } else {
            requestLocationPermissions()
        }
    }
    private fun hasLocationPermission(): Boolean {
        val fineLocationPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        val coarseLocationPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
        return fineLocationPermission == PackageManager.PERMISSION_GRANTED && coarseLocationPermission == PackageManager.PERMISSION_GRANTED
    }

    private fun requestLocationPermissions() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), requestCode)
    }

    private fun startRepeatingTask() {
        handler.post(runnable)
    }

    private val runnable = object : Runnable {
        override fun run() {
            viewModel.fetchLocation(fusedLocation,db)
            handler.postDelayed(this, delayMillis)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == requestCode) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                startRepeatingTask()
            } else {
                if (permissionRequestCount < maxPermissionRequests) {
                    requestLocationPermissions()
                    permissionRequestCount++
                } else {
                    showCustomSnackbar()
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("permissionRequestCount", permissionRequestCount)
    }

    private fun showCustomSnackbar() {
        val view = findViewById<View>(android.R.id.content)
        val snackbar = Snackbar.make(view, "Es necesario aceptar los permisos para continuar con el proceso, AGREGALOS MANUALMENTE", Snackbar.LENGTH_INDEFINITE)
        snackbar.setAction("Cerrar") { snackbar.dismiss() }
        snackbar.show()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap


        viewModel.data.observe(this, Observer{locationList ->
            binding.recyclerLocations.layoutManager = LinearLayoutManager(this)
            val adapter = LocationAdapter(locationList)
            binding.recyclerLocations.adapter = adapter
            for (location in locationList) {
                val latLng = LatLng(location.latitude, location.longitude)
                val markerOptions = MarkerOptions().position(latLng).title("Ubicación")
                mMap.addMarker(markerOptions)
            }
            if (locationList.isNotEmpty()) {
                val lastLocation = locationList.last()
                val lastLatLng = LatLng(lastLocation.latitude, lastLocation.longitude)
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lastLatLng, 10f))
            }
        })
    }

}