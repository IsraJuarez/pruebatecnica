package com.freektzon.pruebatecnica.utils

import android.content.Context
import android.view.View
import android.widget.ProgressBar

class MyLoader(private val progress: ProgressBar) {
    private val progressBar: ProgressBar = progress

    init {
        progressBar.visibility = View.GONE
    }

    fun show(){
        progressBar.visibility = View.VISIBLE
    }
    fun hide(){
        progressBar.visibility = View.GONE
    }
}