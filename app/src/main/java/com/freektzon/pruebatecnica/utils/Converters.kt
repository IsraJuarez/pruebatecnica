package com.freektzon.pruebatecnica.utils

import android.util.Log
import androidx.room.TypeConverter
import com.freektzon.pruebatecnica.objects.Sprites
import com.freektzon.pruebatecnica.objects.Types
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class Converters {
    @TypeConverter
    fun fromSprites(sprites: Sprites?): String? {
        return sprites?.let {
            Gson().toJson(it)
        }
    }

    @TypeConverter
    fun toSprites(spritesJson: String?): Sprites? {
        return spritesJson?.let {
            Gson().fromJson(it, Sprites::class.java)
        }
    }

    @TypeConverter
    fun fromTypesList(types: ArrayList<Types>?): String? {
        val gson = Gson()
        val type = object : TypeToken<ArrayList<Types>>() {}.type
        return gson.toJson(types, type)
    }

    @TypeConverter
    fun toTypesList(typesString: String?): ArrayList<Types> {
        return if (!typesString.isNullOrEmpty()) {
            val gson = Gson()
            val type = object : TypeToken<ArrayList<Types>>() {}.type
            gson.fromJson(typesString, type)
        } else {
            ArrayList()
        }
    }
}