package com.freektzon.pruebatecnica.utils

class Constants {

    companion object{
        const val BASE_URL = "https://pokeapi.co/"
        const val BASE_URL_IMAGE = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"
    }
}