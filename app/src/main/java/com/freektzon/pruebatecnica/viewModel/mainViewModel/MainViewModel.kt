package com.freektzon.pruebatecnica.viewModel.mainViewModel

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.freektzon.pruebatecnica.db.getDataBase
import com.freektzon.pruebatecnica.objects.Poke
import com.freektzon.pruebatecnica.objects.Results
import com.freektzon.pruebatecnica.objects.Type
import com.freektzon.pruebatecnica.objects.Types
import com.freektzon.pruebatecnica.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel(private val application: Application, private val repository: Repository) :
    ViewModel() {

    private var _data = MutableLiveData<List<Poke>>()
    private val db = getDataBase(application)
    private val pokemonDao = db.pokemonDao()
    private var _status = MutableLiveData<Boolean>()
    val status: LiveData<Boolean>
        get() = _status
    val data: LiveData<List<Poke>>
        get() = _data
    private var counter: Int = 0
    private var outset: Int = 0
    private var limit: Int = 25

    fun fetchData() {
        _status.postValue(true)
        if (isInternetAvailable(application)) {
            viewModelScope.launch {
                if (outset <= 1292) {
                    repository.fetchData(outset, limit, object : Callback<Results> {
                        override fun onResponse(call: Call<Results>, response: Response<Results>) {
                            response.body().also { results ->
                                results?.results?.let { newResult ->
                                    viewModelScope.launch {
                                        withContext(Dispatchers.IO) {
                                            for (item in newResult) {
                                                val type = "defaultType"
                                                val types = Type(type)
                                                val typess = Types(types)
                                                val listTypes = ArrayList<Types>()
                                                listTypes.add(typess)
                                                item.types = listTypes
                                            }
                                            pokemonDao.insert(newResult)
                                            getListPokemon()
                                        }
                                        _status.postValue(false)
                                    }
                                    counter++
                                    outset += 25
                                }
                            }
                        }

                        override fun onFailure(call: Call<Results>, t: Throwable) {
                            if (_data.value.isNullOrEmpty()) {
                                viewModelScope.launch {
                                    withContext(Dispatchers.IO) {
                                        getListPokemon()
                                    }
                                }
                            }
                            _status.postValue(false)
                        }
                    })
                }
            }
        } else {
            if (_data.value.isNullOrEmpty()) {
                viewModelScope.launch {
                    withContext(Dispatchers.IO) {
                        getListPokemon()
                    }
                }
            }
            _status.postValue(false)
        }
    }

    fun updateStatusFavorite(poke: Poke) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                pokemonDao.updatePokemon(poke)
            }
        }
    }

    fun getListPokemon() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _data.postValue(pokemonDao.getAllPokemon())
            }
        }
    }

    private fun isInternetAvailable(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val network = connectivityManager.activeNetwork
        val networkCapabilities = connectivityManager.getNetworkCapabilities(network)
        return networkCapabilities?.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) == true
    }
}