package com.freektzon.pruebatecnica.viewModel.locationViewModel

import android.annotation.SuppressLint
import android.location.Location
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.freektzon.pruebatecnica.objects.LocationData
import com.freektzon.pruebatecnica.objects.Poke
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import java.text.SimpleDateFormat
import java.util.Date
import java.util.TimeZone

class LocationViewModel: ViewModel() {

    private var _data = MutableLiveData<List<LocationData>>()
    val data: LiveData<List<LocationData>>
        get() = _data
    @SuppressLint("MissingPermission")
    fun fetchLocation(fusedLocation: FusedLocationProviderClient, db: FirebaseFirestore){
        fusedLocation.lastLocation
            .addOnSuccessListener { location: Location? ->
                if (location != null) {
                    val locationData = hashMapOf(
                        "latitude" to location.latitude,
                        "longitude" to location.longitude,
                        "timestamp" to FieldValue.serverTimestamp()
                    )
                    db.collection("myLocation").add(locationData)
                        .addOnSuccessListener {
                            getLocationes(db)
                        }
                        .addOnFailureListener { e ->
                            Log.e("TAG", "no subió ${e.message}")
                        }
                }
            }
    }

    fun getLocationes(db: FirebaseFirestore){
        db.collection("myLocation")
            .get()
            .addOnSuccessListener { querySnapshot ->
                val locationList = mutableListOf<LocationData>()
                val listString = mutableListOf<String>()

                for (document in querySnapshot) {
                    // Accede a los datos de cada documento
                    val latitude = document.getDouble("latitude")
                    val longitude = document.getDouble("longitude")
                    val timestamp = document.get("timestamp")

                    // Crea un objeto de datos de ubicación
                    if (latitude != null && longitude != null && timestamp != null) {
                        val location = LocationData(latitude, longitude, convertirTimestampAFechaHora(timestamp))
                        locationList.add(location)
                        listString.add(location.toString())
                    }
                }
                _data.postValue(locationList)
            }
            .addOnFailureListener { e ->
                println("Error al obtener las ubicaciones: ${e.message}")
            }
    }
    private fun convertirTimestampAFechaHora(timestamp: Any): String {
        if (timestamp is Timestamp) {
            val segundos = timestamp.seconds
            val nanosegundos = timestamp.nanoseconds

            val milisegundos = (segundos * 1000) + (nanosegundos / 1_000_000)
            val date = Date(milisegundos)
            val sdf = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
            sdf.timeZone = TimeZone.getDefault()
            return sdf.format(date)
        } else {
            return "Fecha y hora no válidas"
        }
    }
}