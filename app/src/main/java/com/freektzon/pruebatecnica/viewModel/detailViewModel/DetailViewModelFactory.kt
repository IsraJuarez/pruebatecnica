package com.freektzon.pruebatecnica.viewModel.detailViewModel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.freektzon.pruebatecnica.repository.Repository

class DetailViewModelFactory (private val application: Application, private val repository: Repository, private val url:String, private val name:String): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DetailViewModel::class.java)) {
            return DetailViewModel(application,repository,url,name) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}