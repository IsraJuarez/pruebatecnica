package com.freektzon.pruebatecnica.viewModel.mainViewModel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.room.RoomDatabase
import com.freektzon.pruebatecnica.db.PokemonDatabase
import com.freektzon.pruebatecnica.repository.Repository

class ViewModelFactory(private val application:Application, private val repository: Repository):ViewModelProvider.Factory {

        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
                return MainViewModel(application,repository) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }

}