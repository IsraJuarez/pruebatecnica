package com.freektzon.pruebatecnica.viewModel.detailViewModel

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.freektzon.pruebatecnica.db.getDataBase
import com.freektzon.pruebatecnica.objects.Poke
import com.freektzon.pruebatecnica.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailViewModel(private val application: Application, private val repository: Repository, private val url:String, private val name:String): ViewModel() {

    private var _data = MutableLiveData<Poke>()
    private var db = getDataBase(application)
    var pokemonDao = db.pokemonDao()
    private var _status = MutableLiveData<Boolean>()
    val data: LiveData<Poke>
        get() = _data
    val status: LiveData<Boolean>
        get() = _status
    @SuppressLint("SuspiciousIndentation")
    fun getDetailsPoke(){
        _status.postValue(true)
        if (isInternetAvailable(application)){
            viewModelScope.launch {
                val parts = url.split("/")
                val id = parts[parts.size - 2]
                repository.getDetails(id.toInt() ,object : Callback<Poke> {
                    override fun onResponse(call: Call<Poke>, response: Response<Poke>) {
                        response.body().also {results ->
                            results?.let { newResult ->
                                viewModelScope.launch {
                                    withContext(Dispatchers.IO){
                                        val pokemon = pokemonDao.getPokemonByName(newResult.name)
                                        pokemon.weight = newResult.weight
                                        pokemon.height = newResult.height
                                        pokemon.types = newResult.types
                                        pokemon.sprites = newResult.sprites
                                        pokemonDao.updatePokemon(pokemon)
                                        _data.postValue(pokemon)
                                    }
                                }
                            }
                            _status.postValue(false)
                        }
                    }
                    override fun onFailure(call: Call<Poke>, t: Throwable) {
                        viewModelScope.launch {
                            withContext(Dispatchers.IO){
                                _data.postValue(pokemonDao.getPokemonByName(name))
                            }
                            _status.postValue(false)
                        }
                    }
                })
            }
        }else{
            viewModelScope.launch {
                withContext(Dispatchers.IO){
                    _data.postValue(pokemonDao.getPokemonByName(name))
                }
            }
            _status.postValue(false)
        }
    }

    fun updateStatusFavorite(poke: Poke){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                val pokemonDao = db.pokemonDao()
                pokemonDao.updatePokemon(poke)
            }
        }
    }

    fun isInternetAvailable(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val network = connectivityManager.activeNetwork
        val networkCapabilities = connectivityManager.getNetworkCapabilities(network)
        return networkCapabilities?.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) == true
    }
}