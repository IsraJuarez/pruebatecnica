package com.freektzon.pruebatecnica.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.freektzon.pruebatecnica.objects.Poke

@Dao
interface PokemonDao {
    @Query("SELECT * FROM pokemon WHERE name = :namePoke")
    fun getPokemonByName(namePoke: String): Poke
    @Query("DELETE FROM pokemon")
    fun deleteAll()
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(pokemon: List<Poke>)

    @Query("SELECT * FROM pokemon")
    fun getAllPokemon(): MutableList<Poke>

    @Update
    fun updatePokemon(pokemon: Poke)
}