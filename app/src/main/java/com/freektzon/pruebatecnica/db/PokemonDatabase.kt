package com.freektzon.pruebatecnica.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.freektzon.pruebatecnica.objects.Poke
import com.freektzon.pruebatecnica.utils.Converters

@Database(entities = [Poke::class], version = 3)
@TypeConverters(Converters::class)
abstract class PokemonDatabase: RoomDatabase() {
    abstract fun pokemonDao(): PokemonDao
}

private lateinit var INSTANCE: PokemonDatabase

fun getDataBase(context: Context): PokemonDatabase{
    synchronized(PokemonDatabase::class.java){
        if (!::INSTANCE.isInitialized){
            INSTANCE = Room.databaseBuilder(
                context.applicationContext,
                PokemonDatabase::class.java,
                "pokes_db"
            ).build()
        }
        return INSTANCE
    }
}