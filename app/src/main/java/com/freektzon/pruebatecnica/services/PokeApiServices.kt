package com.freektzon.pruebatecnica.services

import com.freektzon.pruebatecnica.objects.Poke
import com.freektzon.pruebatecnica.objects.Results
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PokeApiServices {
    @GET("api/v2/pokemon/?")
    fun getPokes(@Query("offset")limit:Int,@Query("limit")initial:Int): Call<Results>

    @GET("api/v2/pokemon/{id}/")
    fun getDetailsPoke(@Path("id")id:Int): Call<Poke>

}