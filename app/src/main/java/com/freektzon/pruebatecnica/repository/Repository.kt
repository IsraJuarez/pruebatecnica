package com.freektzon.pruebatecnica.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.freektzon.pruebatecnica.objects.Poke
import com.freektzon.pruebatecnica.objects.Results
import com.freektzon.pruebatecnica.services.PokeApiServices
import okhttp3.HttpUrl
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Repository(private val apiService: PokeApiServices) {
    fun fetchData(initial:Int,limit:Int,call:Callback<Results>){
        apiService.getPokes(initial,limit).enqueue(call)
    }

    fun getDetails(id:Int,call:Callback<Poke>){
        apiService.getDetailsPoke(id).enqueue(call)
    }
}